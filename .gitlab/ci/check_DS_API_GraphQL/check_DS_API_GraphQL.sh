#!/bin/bash

set -o errexit

##########################################################################################
# shellcheck disable=SC2034
TEMP=$(getopt -o cpt --long help --long token: --long url: --long procedure: --long retry: -n 'javawrap' -- "$@")
#TEMP=$(getopt -o cpt --long help --long token: --long url: --long procedure: -n 'javawrap' -- "$@")

##########################################################################################
usage() {
    echo ""
    echo " DS - Check if GraphQL server is up and GraphQL token is still valid"
    echo " --------------------------------------------------------------------"
    echo " usage: ${0} [OPTIONS]..."
    echo ""
    echo "   --url       <arg>   GraphQL URL endpoint"
    echo "   --token     <arg>   GraphQL token"
    echo "   --procedure <arg>   Procedure ID"
    echo "   --retry     <arg>   number of retry if HTTP request fails"
    echo "   --help              Display documentation."
    echo ""
    echo " --------------------------------------------------------------------"
    echo ""
    exit 2
}

# Process the parameters
if [[ "${1}" == "" ]]; then
    usage
fi
declare GRAPHQL_URL=
declare GRAPHQL_TOKEN=
declare PROCEDURE_ID=
while true; do
    case "$1" in
    --help)
        usage
        break ;;
    --url)
        GRAPHQL_URL="$2"
        shift 2
        ;;
    --token)
        GRAPHQL_TOKEN="$2"
        shift 2
        ;;
    --procedure)
        PROCEDURE_ID="$2"
        shift 2
        ;;
    --retry)
        RETRY="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done

# Check mandatory parameters
if [[ "${GRAPHQL_URL}" == "" ]]; then
    echo "Error : GraphQL URL is missing"
    usage
fi
if [[ "${GRAPHQL_TOKEN}" == "" ]]; then
    echo "Error : GraphQL token is missing"
    usage
fi
if [[ "${PROCEDURE_ID}" == "" ]]; then
    echo "Error : procedure ID is missing"
    usage
fi
if [[ "${RETRY}" == "" ]]; then
    RETRY=5
else
    RETRY=$(echo "${RETRY}" | sed "s/[^0-9]//g")
fi

# Request to GraphQL API of DS
if [[ ! -f "./EXPECTED_API_RESPONSE.json" ]]; then
    echo "ERROR - File not found: ./EXPECTED_API_RESPONSE.json"
    usage
fi


# Request to GraphQL API of DS
DS_REQUEST="{ demarche(number: ${PROCEDURE_ID}) { state title dateCreation datePublication description service { typeOrganisme organisme nom } } }"
echo "allowed retry: ${RETRY}"
CURL_EXIT_CODE=""
for (( i=1; i <= "${RETRY}"; i++ ))
do
    if [[ "${CURL_EXIT_CODE}" == "0" ]]; then
        break
    else
        if [[ "${i}" -gt 1 ]]; then
            echo "wait ${i}  seconds before next retry"
            sleep "${i}"
        fi
        echo ""; echo "### Trial number ${i} #######"
        set +o errexit
        curl   --fail                 \
               --silent               \
               --show-error           \
               --connect-timeout   5  \
               --max-time         10  \
               --request    POST      \
               --header "Content-Type: application/json"                 \
               --header "Authorization: Bearer token=${GRAPHQL_TOKEN}"   \
               --data   "{ \"query\": \"${DS_REQUEST}\" }"               \
               --output "CURRENT_API_RESPONSE.tmp"                       \
               "${GRAPHQL_URL}"
           CURL_EXIT_CODE=$?
           set -o errexit
           ####################################################################################################################
           # Documentation: https://curl.se/docs/manpage.html
           #    -f, --fail         Turn 4xx HTTP error code to CURL error (22 - HTTP page not retrieved)
           #    -s, --silent       Silent or quiet mode. Do not show progress meter or error messages.
           #    -S, --show-error   When used with -s, --silent, it makes curl show an error message if it fails.
           #    --connect-timeout  Maximum time in seconds that you allow curl's connection to take.
           #                       This only limits the connection phase, so if curl connects within the given period it will continue - if not it will exit.
           #    --max-time 10      Maximum time, in seconds, that we allow command line to spend before curl exits with a timeout error code (28)
           ####################################################################################################################
    fi
done

# Check if HTTP request has not failed
if [[ "${CURL_EXIT_CODE}" != 0 ]]; then
    echo "---> ERROR"
    echo "---> CURL failed to retrieve the GraphQL response"
    exit 1
fi

# Compare expected and current GraphQL responses
CURRENT_API_RESPONSE="$(cat CURRENT_API_RESPONSE.tmp)"
EXPECTED_API_RESPONSE="$(cat EXPECTED_API_RESPONSE.json)"
rm "CURRENT_API_RESPONSE.tmp"
echo "EXPECTED_API_RESPONSE"
echo "${EXPECTED_API_RESPONSE}" && echo ""
echo "CURRENT_API_RESPONSE"
echo "${CURRENT_API_RESPONSE}"  && echo ""
if [[ "${CURRENT_API_RESPONSE}" == "${EXPECTED_API_RESPONSE}" ]]; then
    echo "---> GraphQL server is up"
    echo "---> GraphQL token is still valid"
    exit 0
else
    echo "---> ERROR"
    echo "---> GraphQL server is down or GraphQL token is not valid."
    exit 1
fi
