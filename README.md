# DS monitoring

## DS - Webapp

every 5 minutes by `uptimerobot.com`

- [DS - **Home page** monitoring](https://stats.uptimerobot.com/L5O2LiPXR4/786521902)
- [DS - **Login page** monitoring](https://stats.uptimerobot.com/L5O2LiPXR4/786521918)

## DS - GraphQL API

[DS - **GraphQL API** monitoring](https://gitlab.adullact.net/demarches-simplifiees/adullact/ds-monitoring/-/pipelines?ref=check_API_GraphQL_schedules&source=schedule)
(every hour)

- check if GraphQL server is up
- check if GraphQL token is still valid

## External APIs used by DS

### External APIs with status page

- [**API Entreprise** status page](https://status.entreprise.api.gouv.fr/)
- [**API Particulier** status page](https://status.particulier.api.gouv.fr/)

### External APIs without status page

- [API Geo](https://api.gouv.fr/les-api/api-geo)
- [API Adresse](https://api.gouv.fr/les-api/base-adresse-nationale)
- [API annuaire de l’Education Nationale](https://api.gouv.fr/les-api/api-annuaire-education)
- [FranceConnect](https://api.gouv.fr/les-api/franceconnect)
- ...


## Documentation

### DS - GraphQL monitoring

- [`.gitlab/ci/check_DS_API_GraphQL/`](.gitlab/ci/check_DS_API_GraphQL)
- [`.gitlab/ci/job.ds.check-graphql.gitlab-ci.yml`](.gitlab/ci/job.ds.check-graphql.gitlab-ci.yml)

## License

[AGPL v3](LICENSE)
