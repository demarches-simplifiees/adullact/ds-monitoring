# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

----------------

## v1.0.0  - 2022-04-05

### Added

- CI: add DS GraphQL monitoring --> #2
- CI: add Docker image of prerequisites to use CURL
- CI: add job to check that internal and external URLs in Markdown files are alive
- CI: enable markdown linter
- CI: add minimal Gitlab CI (hadolint, markdownlint, yamllint and ShellCheck)
- DOC: improve README
- DOC: add base files (README, LICENSE, CHANGELOG, ...)
- CHORE: add technical files (.gitinore, .editorconfig, ...)

## Template

```markdown
## Unreleased yet ---> 1.0.0  - 2021-0x-xx


### Added


### Changed


### Fixed


### Deprecated


### Security
```
