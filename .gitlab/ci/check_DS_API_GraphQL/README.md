# DS - GraphQL monitoring

## Save expected API response

see: [EXPECTED_API_RESPONSE.json](./EXPECTED_API_RESPONSE.json)

```bash
# DS token for the GraphQL API
set +o history         # disable command line history
DS_API_TOKEN="<YOUR_TOKEN>"
set -o history         # enable command line history

# URL of GraphQL server and procedure ID
DS_API_URL="https://ds.example.org/api/v2/graphql"
DS_PROCEDURE_ID="<YOUR_PROCEDURE_ID>"

# Request to GraphQL API of DS
DS_REQUEST="{ demarche(number: ${DS_PROCEDURE_ID}) { state title dateCreation datePublication description service { typeOrganisme organisme nom } } }"
curl  -X POST                                       \
      -H "Content-Type: application/json"           \
      -H "Authorization: Bearer token=${DS_API_TOKEN}" \
      --data "{ \"query\": \"${DS_REQUEST}\" }"     \
      ${DS_API_URL}  > EXPECTED_API_RESPONSE.json
```

## Checks if GraphQL server is up and GraphQL token is still valid

```bash
# DS token for the GraphQL API
set +o history         # disable command line history
DS_API_TOKEN="<YOUR_TOKEN>"
set -o history         # enable command line history

# URL of GraphQL server and procedure ID
DS_API_URL="https://ds.example.org/api/v2/graphql"
DS_PROCEDURE_ID="<YOUR_PROCEDURE_ID>"

# Checks if GraphQL server is up and GraphQL token is still valid
cd .gitlab/ci/check_DS_API_GraphQL/
./check_DS_API_GraphQL.sh --procedure "${DS_PROCEDURE_ID}" --url ${DS_API_URL} --token "${DS_API_TOKEN}"
```

## Configure Gitlab CI

Following Gitlab CI variables must be declared in repository configuration:
- `DS_API_TOKEN`     ----> Protected + Masked
- `DS_API_URL`       ----> Protected + Masked
- `DS_PROCEDURE_ID`  ----> Protected (note: "Masked" flag does not work --> too short)

Note:
> -  **Protected**: only exposed to protected branches or tags.
> -  **Masked**:    hidden in job logs. Must match masking [requirements](https://gitlab.adullact.net/help/ci/variables/README#mask-a-cicd-variable).
